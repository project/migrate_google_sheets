<?php

namespace Drupal\migrate_google_sheets\Plugin\migrate_plus\data_parser;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Obtain Google Sheet data for migration.
 *
 * @DataParser(
 *   id = "google_sheets",
 *   title = @Translation("Google Sheets")
 * )
 */
class GoogleSheets extends Json implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Array of headers from the first row.
   *
   * @var array
   */
  protected $headers = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getSourceData(string $url, string|int $item_selector = '') {
    $parsed_url = UrlHelper::parse($url);

    // Add API key from config if it exists and a key isn't already in the URL.
    $api_key = $this->configFactory->get('migrate_google_sheets.settings')->get('api_key');
    if ($api_key && !in_array('key', array_keys($parsed_url['query']))) {
      $parsed_url['query'] = array_merge($parsed_url['query'], ['key' => $api_key]);
      $url = Url::fromUri(urldecode($parsed_url['path']), $parsed_url)->toString();
    }

    // Since we're being explicit about the data location, we can return the
    // array without calling getSourceIterator to get an iterator to find the
    // correct values.
    try {
      $response = $this->getDataFetcherPlugin()->getResponseContent($url);
      // The TRUE setting means decode the response into an associative array.
      $array = json_decode($response, TRUE);

      // For Google Sheets, the actual row data lives under table->rows.
      if (isset($array['values'])) {
        // Set headers from first row.
        $first_row = array_shift($array['values']);
        $columns = $first_row;

        $this->headers = array_map(function ($col) {
          return strtolower($col);
        }, $columns);

        $array = $array['values'];
      }
      else {
        $array = [];
      }

      return $array;
    }
    catch (RequestException $e) {
      throw new MigrateException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchNextRow(): void {
    $current = $this->iterator->current();
    if ($current) {
      foreach ($this->fieldSelectors() as $field_name => $selector) {
        // Actual values are stored in c[<column index>]['v'].
        $column_index = array_search(strtolower($selector), $this->headers);
        if ($column_index >= 0 && isset($current[$column_index])) {
          $this->currentItem[$field_name] = $current[$column_index];
        }
        else {
          $this->currentItem[$field_name] = '';
        }
      }
      $this->iterator->next();
    }
  }

}
